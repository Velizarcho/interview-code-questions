package others.easy;

import java.util.Stack;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/99/others/721/
public class ValidParentheses {
    public static void main(String[] args) {
        String s = "]";
        System.out.println(isValid(s));
    }

    public static boolean isValid(String s) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            char currentSymbol = s.charAt(i);
            if (currentSymbol == '(' || currentSymbol == '{' || currentSymbol == '[') {
                stack.push(currentSymbol);
                continue;
            }

            if (currentSymbol == ')' || currentSymbol == '}' || currentSymbol == ']') {
                if (stack.empty()) {
                    return false;
                }

                char current = stack.pop();
                switch (current) {
                    case '(':
                        if (currentSymbol != ')') {
                            return false;
                        }
                        break;
                    case '{':
                        if (currentSymbol != '}') {
                            return false;
                        }
                        break;
                    case '[':
                        if (currentSymbol != ']') {
                            return false;
                        }
                        break;
                }
            }
        }

        return stack.empty();
    }
}
