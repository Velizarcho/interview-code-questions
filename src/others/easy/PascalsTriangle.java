package others.easy;

import java.util.ArrayList;
import java.util.List;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/99/others/601/
public class PascalsTriangle {
    public static void main(String[] args) {
        int numRows = 6;
        System.out.println(generate(numRows));
    }

    public static List<List<Integer>> generate(int numRows) {
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> root = List.of(1);
        result.add(root);

        if (numRows == 1) {
            return result;
        }

        List<Integer> secondLevel = List.of(1, 1);
        result.add(secondLevel);

        if (numRows == 2) {
            return result;
        }

        for (int i = 2; i < numRows; i++) {
            List<Integer> currentLevel = new ArrayList<>();
            currentLevel.add(1);
            List<Integer> previousLevel = result.get(i - 1);
            for (int j = 0; j < previousLevel.size() - 1; j++) {
                currentLevel.add(previousLevel.get(j) + previousLevel.get(j + 1));
            }
            currentLevel.add(1);
            result.add(currentLevel);
        }

        return result;
    }
}
