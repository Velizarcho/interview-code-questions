package others.easy;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/99/others/565/
public class NumberOf1Bits {
    public static void main(String[] args) {
        int n = 15;
        System.out.println(hammingWeight(n));
    }

    public static int hammingWeight(int n) {
        int result = 0;
        while (n != 0) {
            if (n % 2 == 1) {
                result++;
            }

            n /= 2;
        }

        return result;
    }

    // Another way with bitwise operations
//    public static int hammingWeight(int n) {
//        int result = 0;
//
//        while (n != 0) {
//            result += (n & 1);
//            n >>>= 1;
//        }
//        return result;
//    }
}
