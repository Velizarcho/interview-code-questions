package others.easy;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/99/others/722/
public class MissingNumber {
    public static void main(String[] args) {
        int[] nums = {0, 1};
        System.out.println(missingNumber(nums));
    }

    public static int missingNumber(int[] nums) {
        int n = nums.length;
        int expectedSum = n * (n + 1) / 2;
        int actualSum = 0;

        for (int num : nums) {
            actualSum += num;
        }

        return expectedSum - actualSum;
    }

    // This works but it's not O(n) for time complexity and O(1) for space complexity
    // It's O(n^2) for time complexity and O(n) for space complexity
//    public static int missingNumber(int[] nums) {
//        int n = nums.length;
//        List<Integer> list = new ArrayList<>();
//        for (int i = 0; i <= n; i++) {
//            list.add(i);
//        }
//
//        for (int i = 0; i < n; i++) {
//            list.remove((Object) nums[i]);
//        }
//
//        return list.get(0);
//    }
}
