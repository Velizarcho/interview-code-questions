package others.easy;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/99/others/648/
public class ReverseBits {
    public static void main(String[] args) {
        int n = 43261596;
        System.out.println(reverseBits(n));
    }

    // TODO: Should revise at later stage
    // you need treat n as an unsigned value
    public static int reverseBits(int n) {
        int result = 0;

        // Loop through all 32 bits
        for (int i = 0; i < 32; i++) {
            // Left shift result to make room for the next bit
            result <<= 1;
            // If the least significant bit of n is 1, add it to result
            result |= (n & 1);
            // Right shift n to process the next bit
            n >>= 1;
        }

        return result;
    }
}
