package others.easy;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/99/others/762/
public class HammingDistance {
    public static void main(String[] args) {
        int x = 4;
        int y = 14;
        System.out.println(hammingDistance(x, y));
    }

    public static int hammingDistance(int x, int y) {
        // XOR the two numbers. This will give a number where the bits are set
        // to 1 only in positions where x and y have different bits.
        int xor = x ^ y;

        // Count the number of set bits (1s) in the XOR result.
        int count = 0;
        while (xor != 0) {
            // Increase count if the least significant bit is 1
            count += xor & 1;
            // Right shift xor by 1 to check the next bit
            xor >>= 1;
        }

        return count;
    }

    // This is working, but with bitwise operations is faster
//    public static int hammingDistance(int x, int y) {
//        List<Integer> firstBinary = new ArrayList<>();
//        List<Integer> secondBinary = new ArrayList<>();
//
//        if (x == 0) {
//            firstBinary.add(0);
//        } else {
//            while (x != 0) {
//                firstBinary.add(x % 2);
//                x /= 2;
//            }
//        }
//
//        if (y == 0) {
//            secondBinary.add(0);
//        } else {
//            while (y != 0) {
//                secondBinary.add(y % 2);
//                y /= 2;
//            }
//        }
//
//        int result = 0;
//        int firstSize = firstBinary.size();
//        int secondSize = secondBinary.size();
//        int minSize = Math.min(firstSize, secondSize);
//
//        for (int i = 0; i < minSize; i++) {
//            if (firstBinary.get(i) != secondBinary.get(i)) {
//                result++;
//            }
//        }
//
//        if (firstSize != secondSize) {
//            if (firstSize < secondSize) {
//                for (int i = minSize; i < secondSize; i++) {
//                    if (secondBinary.get(i) == 1) {
//                        result++;
//                    }
//                }
//            } else {
//                for (int i = minSize; i < firstSize; i++) {
//                    if (firstBinary.get(i) == 1) {
//                        result++;
//                    }
//                }
//            }
//        }
//
//        return result;
//    }
}
