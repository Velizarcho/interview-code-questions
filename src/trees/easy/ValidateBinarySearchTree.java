package trees.easy;

import trees.TreeNode;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/94/trees/625/
public class ValidateBinarySearchTree {
    public static void main(String[] args) {
        TreeNode left = new TreeNode(1);
        TreeNode rightRight = new TreeNode(4);
        TreeNode right = new TreeNode(3, null, rightRight);
        TreeNode root = new TreeNode(2, left, right);
        System.out.println(isValidBST(root));
    }

    public static boolean isValidBST(TreeNode root) {
        return isValidBST(root, null, null);
    }

    private static boolean isValidBST(TreeNode node, Integer minValue, Integer maxValue) {
        if (node == null) {
            return true;
        }

        if (minValue != null && node.val <= minValue) {
            return false;
        }

        if (maxValue != null && node.val >= maxValue) {
            return false;
        }

        return isValidBST(node.left, minValue, node.val) && isValidBST(node.right, node.val, maxValue);
    }
}
