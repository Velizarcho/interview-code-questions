package trees.easy;

import trees.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/94/trees/627/
public class SymmetricTree {
    public static void main(String[] args) {
        TreeNode rightRight = new TreeNode(3);
        TreeNode leftLeft = new TreeNode(3);
        TreeNode right = new TreeNode(2, null, rightRight);
        TreeNode left = new TreeNode(2, leftLeft, null);
        TreeNode root = new TreeNode(1, left, right);
        System.out.println(isSymmetric(root));
    }

    // Recursive way
//    public static boolean isSymmetric(TreeNode root) {
//        if (root == null) {
//            return true;
//        }
//
//        return isSymmetric(root.left, root.right);
//    }
//
//    private static boolean isSymmetric(TreeNode leftSide, TreeNode rightSide) {
//        if (leftSide == null && rightSide == null) {
//            return true;
//        }
//
//        if (leftSide == null || rightSide == null) {
//            return false;
//        }
//
//        return leftSide.val == rightSide.val
//                && isSymmetric(leftSide.left, rightSide.right)
//                && isSymmetric(leftSide.right, rightSide.left);
//    }

    // Iterative way
    public static boolean isSymmetric(TreeNode root) {
        if (root == null) {
            return true;
        }

        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root.left);
        queue.add(root.right);

        while (!queue.isEmpty()) {
            TreeNode leftSide = queue.poll();
            TreeNode rightSide = queue.poll();

            if (leftSide == null && rightSide == null) {
                continue;
            }

            if (leftSide == null || rightSide == null) {
                return false;
            }

            if (leftSide.val != rightSide.val) {
                return false;
            }

            queue.add(leftSide.left);
            queue.add(rightSide.right);

            queue.add(leftSide.right);
            queue.add(rightSide.left);
        }

        return true;
    }
}
