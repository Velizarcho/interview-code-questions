package trees.easy;

import trees.TreeNode;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/94/trees/628/
public class BinaryTreeLevelOrderTraversal {
    public static void main(String[] args) {
        TreeNode left = new TreeNode(2);
        TreeNode rightRight = new TreeNode(5);
        TreeNode rightLeft = new TreeNode(4);
        TreeNode right = new TreeNode(3, rightLeft, rightRight);
        TreeNode root = new TreeNode(1, left, right);
//        TreeNode root = new TreeNode(1);
//        TreeNode root = null;
        System.out.println(levelOrder(root));
    }

    public static List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> result = new LinkedList<>();
        if (root == null) {
            return result;
        }

        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);

        while (!queue.isEmpty()) {
            int height = queue.size();
            List<Integer> currentLevelData = new LinkedList<>();

            for (int i = 0; i < height; i++) {
                TreeNode current = queue.poll();
                if (current != null) {
                    currentLevelData.add(current.val);
                }

                if (current != null && current.left != null) {
                    queue.add(current.left);
                }

                if (current != null && current.right != null) {
                    queue.add(current.right);
                }
            }

            result.add(currentLevelData);
        }

        return result;
    }
}
