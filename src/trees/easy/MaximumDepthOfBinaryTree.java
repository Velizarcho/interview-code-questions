package trees.easy;

import trees.TreeNode;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/94/trees/555/
public class MaximumDepthOfBinaryTree {
    public static void main(String[] args) {
        TreeNode left = new TreeNode(2);
        TreeNode rightRight = new TreeNode(4);
        TreeNode right = new TreeNode(3, null, rightRight);
        TreeNode root = new TreeNode(1, left, right);
        System.out.println(maxDepth(root));
    }

    public static int maxDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }

        int depthLeft = maxDepth(root.left);
        int depthRight = maxDepth(root.right);

        return Math.max(depthLeft, depthRight) + 1;
    }
}
