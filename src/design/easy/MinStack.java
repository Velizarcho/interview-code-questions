package design.easy;

import java.util.Arrays;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/98/design/562/
public class MinStack {
    public static void main(String[] args) {
        MinStackTask minStack = new MinStackTask();

        minStack.push(3);
        minStack.push(5);
        System.out.println("Current min: " + minStack.getMin()); // Output: 3

        minStack.push(2);
        minStack.push(1);
        System.out.println("Current min: " + minStack.getMin()); // Output: 1

        minStack.pop();
        System.out.println("Top element after pop: " + minStack.top()); // Output: 2
        System.out.println("Current min after pop: " + minStack.getMin()); // Output: 2

        minStack.pop();
        System.out.println("Top element after another pop: " + minStack.top()); // Output: 5
        System.out.println("Current min after another pop: " + minStack.getMin()); // Output: 3
    }
}


class MinStackTask {

    int[] stack;
    int[] minStack;
    int size;
    int minSize;
    private static final int INITIAL_CAPACITY = 10;
    private static final int RESIZE_FACTOR = 2;

    public MinStackTask() {
        stack = new int[INITIAL_CAPACITY];
        minStack = new int[INITIAL_CAPACITY];
        size = 0;
        minSize = 0;
    }

    private void resize() {
        int newCapacity = stack.length * RESIZE_FACTOR;
        stack = Arrays.copyOf(stack, newCapacity);
        minStack = Arrays.copyOf(minStack, newCapacity);
    }

    public void push(int val) {
        if (size == stack.length) {
            resize();
        }

        stack[size++] = val;
        if (minSize == 0 || val <= minStack[minSize - 1]) {
            minStack[minSize++] = val;
        }
    }

    public void pop() {
        if (size == 0) {
            throw new RuntimeException("Stack is empty");
        }

        if (stack[size - 1] == minStack[minSize - 1]) {
            minSize--;
        }

        size--;
    }

    public int top() {
        if (size == 0) {
            throw new RuntimeException("Stack is empty");
        }

        return stack[size - 1];
    }

    public int getMin() {
        if (minSize == 0) {
            throw new RuntimeException("Stack is empty");
        }

        return minStack[minSize - 1];
    }
}