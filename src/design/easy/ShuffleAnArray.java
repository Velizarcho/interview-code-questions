package design.easy;

import java.util.Arrays;
import java.util.Random;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/98/design/670/
public class ShuffleAnArray {
    public static void main(String[] args) {
        int[] nums = {1, 2, 3};
        ShuffleAnArrayTask obj = new ShuffleAnArrayTask(nums);

        System.out.println(Arrays.toString(obj.shuffle()));
        System.out.println(Arrays.toString(obj.reset()));
        System.out.println(Arrays.toString(obj.shuffle()));
    }
}

class ShuffleAnArrayTask {

    private int[] originalArray;
    private int[] array;
    private Random randomizer;

    public ShuffleAnArrayTask(int[] nums) {
        originalArray = nums.clone();
        array = nums;
        randomizer = new Random();
    }

    public int[] reset() {
        array = originalArray.clone();
        return array;
    }

    public int[] shuffle() {
        for (int i = array.length - 1; i > 0; i--) {
            int randomInt = randomizer.nextInt(i + 1);
            int temp = array[i];
            array[i] = array[randomInt];
            array[randomInt] = temp;
        }

        return array;
    }
}
