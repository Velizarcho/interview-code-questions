package arrays.easy;

import java.util.HashMap;
import java.util.Map;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/546/
public class TwoSum {
    public static int[] twoSum(int[] nums, int target) {
        int[] result = new int[2];
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int difference = target - nums[i];

            if (map.containsKey(nums[i])) {
                result[0] = map.get(nums[i]);
                result[1] = i;
                return result;
            }

            map.put(difference, i);
        }

        return null;
    }
}
