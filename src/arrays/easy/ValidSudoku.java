package arrays.easy;

import java.util.HashSet;
import java.util.Set;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/769/
public class ValidSudoku {
    public static void main(String[] args) {
        char[][] falseBoard =
                       {{'5', '3', '.', '.', '7', '.', '.', '.', '.'},
                        {'6', '.', '.', '1', '9', '5', '.', '.', '.'},
                        {'.', '9', '8', '.', '.', '1', '.', '6', '.'},
                        {'8', '.', '.', '.', '6', '.', '.', '.', '3'},
                        {'4', '.', '.', '8', '.', '3', '.', '.', '1'},
                        {'7', '.', '.', '.', '2', '.', '.', '.', '6'},
                        {'.', '6', '.', '.', '.', '.', '2', '8', '.'},
                        {'.', '.', '.', '4', '1', '9', '.', '.', '5'},
                        {'.', '.', '.', '.', '8', '.', '.', '7', '9'}};

        //        char[][] trueBoard =
        //                       {{'5', '3', '.', '.', '7', '.', '.', '.', '.'},
        //                        {'6', '.', '.', '1', '9', '5', '.', '.', '.'},
        //                        {'.', '9', '8', '.', '.', '.', '.', '6', '.'},
        //                        {'8', '.', '.', '.', '6', '.', '.', '.', '3'},
        //                        {'4', '.', '.', '8', '.', '3', '.', '.', '1'},
        //                        {'7', '.', '.', '.', '2', '.', '.', '.', '6'},
        //                        {'.', '6', '.', '.', '.', '.', '2', '8', '.'},
        //                        {'.', '.', '.', '4', '1', '9', '.', '.', '5'},
        //                        {'.', '.', '.', '.', '8', '.', '.', '7', '9'}};

        System.out.println(isValidSudoku(falseBoard));
    }

    public static boolean isValidSudoku(char[][] board) {
        Set<String> set = new HashSet<>();

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                char current = board[i][j];
                if (current != '.') {
                    // Check if the number has been seen in the current row
                    if (!set.add(current + " in row " + i)) {
                        return false;
                    }

                    // Check if the number has been seen in the current column
                    if (!set.add(current + " in column " + j)) {
                        return false;
                    }

                    // Check if the number has been seen in the current 3x3 box
                    if (!set.add(current + " in box " + (i / 3) + "-" + (j / 3))) {
                        return false;
                    }
                }
            }
        }

        return true;
    }
}
