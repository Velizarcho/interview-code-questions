package arrays.easy;

import java.util.Arrays;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/770/
public class RotateImage {
    public static void main(String[] args) {
        int[][] matrix = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        rotate(matrix);
        System.out.println(Arrays.deepToString(matrix));
    }

    public static void rotate(int[][] matrix) {
        // Transposing the matrix
        for (int i = 0; i < matrix.length; i++) {
            // matrix[i].length in case the matrix isn't NxN
            for (int j = i + 1; j < matrix[i].length; j++) {
                int temp = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i] = temp;
            }
        }

        // Rotating 90 degrees clockwise
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length / 2; j++) {
                int size = matrix[i].length - j - 1;
                int temp = matrix[i][j];
                matrix[i][j] = matrix[i][size];
                matrix[i][size] = temp;
            }
        }
    }
}
