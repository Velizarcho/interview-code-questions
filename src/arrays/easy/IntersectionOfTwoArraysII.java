package arrays.easy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/674/
public class IntersectionOfTwoArraysII {
    public int[] intersect(int[] nums1, int[] nums2) {
        Map<Integer, Integer> set = new HashMap<>();
        for (int j : nums1) {
            set.merge(j, 1, Integer::sum);
        }

        List<Integer> list = new ArrayList<>();
        for (int j : nums2) {
            Integer temp = set.get(j);
            if (temp != null && temp > 0) {
                list.add(j);
                set.put(j, temp - 1);
            }
        }

        return list.stream()
                   .mapToInt(i -> i)
                   .toArray();
    }
}
