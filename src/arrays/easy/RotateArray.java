package arrays.easy;

import java.util.Arrays;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/646/
// TODO: Maybe I should revisit this in the future
public class RotateArray {
    public static void main(String[] args) {
        int[] example = {1, 2, 3, 4, 5, 6, 7};
        rotate(example, 2);
        System.out.println(Arrays.toString(example));
    }

    public static void rotate(int[] nums, int k) {
        // nums = "----->-->"; k =3
        // result = "-->----->";
        //
        // reverse "----->-->" we can get "<--<-----"
        // reverse "<--" we can get "--><-----"
        // reverse "<-----" we can get "-->----->"
        k %= nums.length;
        reverse(nums, 0, nums.length - 1);
        reverse(nums, 0, k - 1);
        reverse(nums, k, nums.length - 1);

        // This was the naive way, and it timed out on larger input arrays
//        for (int i = 0; i < k; i++) {
//            int lastElement = nums[nums.length - 1];
//
//            for (int j = nums.length - 1; j >= 0; j--) {
//                if(j - 1 >= 0) {
//                    nums[j] = nums[j - 1];
//                }
//            }
//
//            nums[0] = lastElement;
//        }
    }

    public static void reverse(int[] nums, int start, int end) {
        while (start < end) {
            int temp = nums[start];
            nums[start] = nums[end];
            nums[end] = temp;
            start++;
            end--;
        }
    }
}
