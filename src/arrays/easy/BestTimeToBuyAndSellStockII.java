package arrays.easy;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/564/
// TODO: Maybe I should revisit this in the future
public class BestTimeToBuyAndSellStockII {
    public static void main(String[] args) {
//        int[] prices = {7, 1, 5, 3, 6, 4}; // 7
//        int[] prices = {1, 2, 3, 4, 5}; // 4
//        int[] prices = {7, 6, 4, 3, 1}; // 0
        int[] prices = {3, 2, 5, 8, 1, 9}; // 14

        System.out.println(maxProfit(prices));
    }

    public static int maxProfit(int[] prices) {
        int totalProfit = 0;

        // Iterate through the prices
        for (int i = 1; i < prices.length; i++) {
            // If the price has increased compared to the previous day
            if (prices[i] > prices[i - 1]) {
                // Add the profit from this increase to the total profit
                totalProfit += prices[i] - prices[i - 1];
            }
        }

        return totalProfit;
    }
}
