package arrays.easy;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/559/
public class PlusOne {
    public int[] plusOne(int[] digits) {
        if (digits[digits.length - 1] == 9) {
            digits[digits.length - 1] = 10;
            for (int i = digits.length - 1; digits[i] == 10 && i >= 0; i--) {
                digits[i] = 0;
                if (i == 0) {
                    int[] resizedArr = new int[digits.length + 1];
                    resizedArr[0] = 1;

                    return resizedArr;
                }

                digits[i - 1]++;
            }
        } else {
            digits[digits.length - 1]++;
        }

        return digits;
    }
}
