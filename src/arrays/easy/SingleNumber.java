package arrays.easy;

import java.util.HashSet;
import java.util.Set;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/549/
public class SingleNumber {
    public int singleNumber(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < nums.length; i++) {
            if (set.add(nums[i]) == false) {
                set.remove(nums[i]);
            }
        }

        for (int result : set) {
            return result;
        }

        return 0;
    }
}
