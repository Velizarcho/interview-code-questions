package arrays.easy;

import java.util.Arrays;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/727/
public class RemoveDuplicatesFromSortedArray {
    public int removeDuplicates(int[] nums) {
        int counter = 0;
        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i] == nums[i + 1]) {
                nums[i] = 101;
                counter++;
            }
        }

        Arrays.sort(nums);

        return nums.length - counter;
    }
}
