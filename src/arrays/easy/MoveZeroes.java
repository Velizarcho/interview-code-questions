package arrays.easy;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/567/
public class MoveZeroes {
    public void moveZeroes(int[] nums) {
        // This took 264ms
//        for (int i = 1; i < nums.length; i++) {
//            if (nums[i] == 0 || nums[i - 1] != 0) {
//                continue;
//            }
//
//            for (int j = i; j > 0; j--) {
//                if (nums[j] != 0 && nums[j - 1] == 0) {
//                    nums[j] ^= nums[j - 1];
//                    nums[j - 1] ^= nums[j];
//                    nums[j] ^= nums[j - 1];
//                }
//            }
//        }

        // This took 2ms
        int pointer1 = 0;
        int pointer2 = 0;
        while (pointer2 < nums.length) {
            if (nums[pointer1] != 0) {
                pointer1++;
                pointer2++;
            } else {
                if (nums[pointer2] == 0) {
                    pointer2++;
                } else {
                    nums[pointer1] = nums[pointer2];
                    nums[pointer2] = 0;
                    pointer1++;
                    pointer2++;
                }
            }
        }
    }
}
