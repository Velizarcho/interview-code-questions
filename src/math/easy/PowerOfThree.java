package math.easy;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/102/math/745/
public class PowerOfThree {
    public static void main(String[] args) {
        int n = 27;
        System.out.println(isPowerOfThree(n));
    }

    public static boolean isPowerOfThree(int n) {
        if (n <= 0) {
            return false;
        }

        while (n % 3 == 0) {
            n /= 3;
        }

        return n == 1;
    }

    // Without loops/recursions:
    //    public boolean isPowerOfThree(int n) {
    //        // The maximum power of 3 that fits in a signed 32-bit integer is 3^19 = 1162261467
    //        int maxPowerOfThree = 1162261467;
    //
    //        // Check if n is greater than 0 and divides maxPowerOfThree evenly
    //        return n > 0 && maxPowerOfThree % n == 0;
    //    }
}
