package math.easy;

import java.util.Map;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/102/math/878/
public class RomanToInteger {
    public static void main(String[] args) {
//        String s = "III";
//        String s = "LVIII";
        String s = "MCMXCIV";
        System.out.println(romanToInt(s));
    }

    public static int romanToInt(String s) {
        Map<Character, Integer> map = Map.of(
                'I', 1,
                'V', 5,
                'X', 10,
                'L', 50,
                'C', 100,
                'D', 500,
                'M', 1000
        );

        int result = 0;

        for (int i = 0; i < s.length(); i++) {
            int currentNumber = map.get(s.charAt(i));

            if (i + 1 < s.length() && currentNumber < map.get(s.charAt(i + 1))) {
                result -= currentNumber;
            } else {
                result += currentNumber;
            }
        }

        return result;
    }
}
