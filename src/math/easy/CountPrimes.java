package math.easy;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/102/math/744/
public class CountPrimes {
    public static void main(String[] args) {
        int n = 10;
        long startTime = System.nanoTime();
        System.out.println(countPrimes(n));

        long endTime = System.nanoTime();
        long executionTimeInMs = (endTime - startTime) / 1000000;
        System.out.println("Execution time in ms: " + executionTimeInMs);
    }

    public static int countPrimes(int n) {
        if (n <= 2) {
            return 0;
        }

        boolean[] isPrime = new boolean[n];
        for (int i = 2; i < n; i++) {
            isPrime[i] = true;
        }

        for (int i = 2; i * i < n; i++) {
            if (isPrime[i] == true) {
                for (int j = i * i; j < n; j += i) {
                    isPrime[j] = false;
                }
            }
        }

        int count = 0;
        for (int i = 2; i < n; i++) {
            if (isPrime[i]) {
                count++;
            }
        }

        return count;
    }
}
