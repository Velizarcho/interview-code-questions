package math.easy;

import java.util.ArrayList;
import java.util.List;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/102/math/743/
public class FizzBuzz {
    public static void main(String[] args) {
        int n = 15;
        System.out.println(fizzBuzz(n));
    }

    public static List<String> fizzBuzz(int n) {
        List<String> result = new ArrayList<>();
        for (int i = 1; i <= n; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                result.add("FizzBuzz");
                continue;
            }

            if (i % 3 == 0) {
                result.add("Fizz");
                continue;
            }

            if (i % 5 == 0) {
                result.add("Buzz");
                continue;
            }

            result.add(Integer.toString(i));
        }

        return result;
    }
}
