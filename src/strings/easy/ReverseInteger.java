package strings.easy;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/127/strings/880/
public class ReverseInteger {
    public static void main(String[] args) {
//        int example = -123;
//        int example = 1534236469;
        int example = -2147483648;
        System.out.println(example);
        System.out.println(reverse(example));
    }

    public static int reverse(int x) {
        long result = 0;
        while (x != 0) {
            int temp = x % 10;
            result += temp;
            result *= 10;
            x /= 10;
        }

        result /= 10;

        if (result > Integer.MAX_VALUE || result < Integer.MIN_VALUE) {
            return 0;
        }

        return (int) result;
    }
}
