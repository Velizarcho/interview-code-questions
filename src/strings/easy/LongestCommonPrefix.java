package strings.easy;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/127/strings/887/
public class LongestCommonPrefix {
    public static void main(String[] args) {
        String[] strs = {"asd"};
        System.out.println(longestCommonPrefix(strs));
    }

    public static String longestCommonPrefix(String[] strs) {
        int minLength = Integer.MAX_VALUE;
        for (int i = 0; i < strs.length; i++) {
            if (minLength > strs[i].length()) {
                minLength = strs[i].length();
            }
        }

        StringBuilder result = new StringBuilder();
        boolean flag = false;
        for (int i = 0; i < minLength; i++) {
            for (int j = 0; j < strs.length - 1; j++) {
                if (strs[j].charAt(i) != strs[j + 1].charAt(i)) {
                    flag = true;
                    break;
                }
            }

            if (flag == true) {
                return result.toString();
            }

            result.append(strs[0].charAt(i));
        }

        return result.toString();
    }
}
