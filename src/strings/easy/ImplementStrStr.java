package strings.easy;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/127/strings/885/
public class ImplementStrStr {
    public int strStr(String haystack, String needle) {
        int length = needle.length();
        if (haystack.length() < length) {
            return -1;
        }

        for (int i = 0; i + length <= haystack.length(); i++) {
            String str = haystack.substring(i, i + length);
            if (str.equals(needle)) {
                return i;
            }
        }

        return -1;
    }
}
