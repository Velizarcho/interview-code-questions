package strings.easy;

import java.util.LinkedHashMap;
import java.util.Map;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/127/strings/881/
public class FirstUniqueCharacterInAString {
    public static void main(String[] args) {
        String test = "aadadaad";
        System.out.println(firstUniqChar(test));
    }

    public static int firstUniqChar(String s) {
        // TODO: this took 30ms, there are faster answers
        Map<Character, Integer> map = new LinkedHashMap<>();
        for (int i = 0; i < s.length(); i++) {
            if (map.containsKey(s.charAt(i)) == false) {
                map.put(s.charAt(i), i);
            } else {
                map.put(s.charAt(i), -1);
            }
        }

        return map.values().stream()
                           .filter(x -> x != -1)
                           .findFirst()
                           .orElse(-1);
    }
}
