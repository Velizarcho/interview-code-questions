package strings.easy;

import java.util.Arrays;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/127/strings/882/
public class ValidAnagram {
    public boolean isAnagram(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }

        char[] src = s.toCharArray();
        char[] dest = t.toCharArray();
        Arrays.sort(src);
        Arrays.sort(dest);

        for (int i = 0; i < src.length; i++) {
            if (src[i] != dest[i]) {
                return false;
            }
        }

        return true;
    }
}
