package strings.easy;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/127/strings/879/
public class ReverseString {
    public void reverseString(char[] s) {
        int beginning = 0;
        int end = s.length - 1;

        while (beginning <= end) {
            // This didn't work out, because XORing ASCII symbols gives strange results
//            s[beginning] ^= s[end];
//            s[end] ^= s[beginning];
//            s[beginning] ^= s[end];
            char temp = s[beginning];
            s[beginning] = s[end];
            s[end] = temp;
            beginning++;
            end--;
        }
    }
}
