package sortingandsearching.easy;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/96/sorting-and-searching/774/
public class FirstBadVersion {
    public static void main(String[] args) {

    }

    public static int firstBadVersion(int n) {
        int left = 1;
        int right = n;

        while (left < right) {
            int mid = left + ((right - left) / 2);

            // It's implemented in the task, don't mind it
            if (isBadVersion(mid) == true) {
                right = mid;
            } else {
                left = mid + 1;
            }
        }

        return left;
    }

    private static boolean isBadVersion(int mid) {
        return Math.random() % 2 == 0;
    }
}
