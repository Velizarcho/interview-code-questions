package linkedlist.easy;

import linkedlist.ListNode;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/93/linked-list/553/
// This was tricky, because you're not given the beginning (head) of the linked list, so you just "kinda" delete the node
public class DeleteNodeInALinkedList {
    public static void main(String[] args) {
        ListNode a4 = new ListNode(4);
        ListNode a3 = new ListNode(3, a4);
        ListNode a2 = new ListNode(2, a3);
        ListNode headA = new ListNode(1, a2);
        deleteNode(a2);

        while (headA != null) {
            System.out.println(headA.val);
            headA = headA.next;
        }
    }

    public static void deleteNode(ListNode node) {
        node.val = node.next.val;
        node.next = node.next.next;
    }
}
