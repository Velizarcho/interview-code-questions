package linkedlist.easy;

import linkedlist.ListNode;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/93/linked-list/603/
// TODO: Should revise at later stage
public class RemoveNthNodeFromEndOfList {
    public static void main(String[] args) {
        ListNode a6 = new ListNode(6);
        ListNode a5 = new ListNode(5, a6);
        ListNode a4 = new ListNode(4, a5);
        ListNode a3 = new ListNode(3, a4);
        ListNode a2 = new ListNode(2, a3);
        ListNode headA = new ListNode(1, a2);
//        ListNode headA = new ListNode(1);

        ListNode result = removeNthFromEnd(headA, 4);
        while (result != null) {
            System.out.println(result.val);
            result = result.next;
        }
    }

    public static ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode fast = head;
        ListNode slow = head;

        for (int i = 0; i < n; i++) {
            fast = fast.next;
        }

        if (fast == null) {
            return head.next;
        }

        while (fast.next != null) {
            fast = fast.next;
            slow = slow.next;
        }

        slow.next = slow.next.next;

        return head;
    }
}
