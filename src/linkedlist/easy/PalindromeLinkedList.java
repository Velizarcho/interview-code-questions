package linkedlist.easy;

import linkedlist.ListNode;

import java.util.ArrayList;
import java.util.List;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/93/linked-list/772/
// TODO: Should revise at later stage
public class PalindromeLinkedList {
    public static void main(String[] args) {
//        ListNode a4 = new ListNode(1);
//        ListNode a3 = new ListNode(2, a4);
//        ListNode a2 = new ListNode(2, a3);
//        ListNode headA = new ListNode(1, a2);
        ListNode a3 = new ListNode(2);
        ListNode a2 = new ListNode(2, a3);
        ListNode headA = new ListNode(1, a2);
        System.out.println(isPalindrome(headA));
    }

    public static boolean isPalindrome(ListNode head) {
        List<Integer> values = new ArrayList<>();
        while (head != null) {
            values.add(head.val);
            head = head.next;
        }

        for (int i = 0; i < values.size() / 2; i++) {
            if (values.get(i) != values.get(values.size() - i - 1)) {
                return false;
            }
        }

        return true;
    }
}
