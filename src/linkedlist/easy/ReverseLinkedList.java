package linkedlist.easy;

import linkedlist.ListNode;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/93/linked-list/560/
public class ReverseLinkedList {
    public static void main(String[] args) {
        ListNode a1 = new ListNode(3);
        ListNode a2 = new ListNode(2, a1);
        ListNode head = new ListNode(1, a2);
        System.out.println(head.val);
        System.out.println(head.next.val);
        System.out.println(head.next.next.val);
        reverseList(head);
        System.out.println("~~~~~~~~~~~~~~~");
        System.out.println(a1.val);
        System.out.println(a1.next.val);
        System.out.println(a1.next.next.val);
    }

    public static ListNode reverseList(ListNode head) {
        ListNode prev = null;
        while (head != null) {
            ListNode next = head.next;
            head.next = prev;
            prev = head;
            head = next;
        }

        return prev;
    }
}
