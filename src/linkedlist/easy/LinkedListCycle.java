package linkedlist.easy;

import linkedlist.ListNode;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/93/linked-list/773/
public class LinkedListCycle {
    public static void main(String[] args) {
        ListNode a4 = new ListNode(4);
        ListNode a3 = new ListNode(3, a4);
        ListNode a2 = new ListNode(2, a3);
        ListNode headA = new ListNode(1, a2);
        a4.next = headA;
        System.out.println(hasCycle(headA));
    }

    public static boolean hasCycle(ListNode head) {
        ListNode slow = head;
        ListNode fast = head;

        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;

            if (slow == fast) {
                return true;
            }
        }

        return false;
    }
}
