package linkedlist.easy;

import linkedlist.ListNode;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/93/linked-list/771/
public class MergeTwoSortedLists {
    public static void main(String[] args) {
        ListNode a3 = new ListNode(4);
        ListNode a2 = new ListNode(2, a3);
//        ListNode a2 = new ListNode(2);
        ListNode headA = new ListNode(1, a2);

        ListNode b3 = new ListNode(4);
        ListNode b2 = new ListNode(3, b3);
//        ListNode b2 = new ListNode(3);
        ListNode headB = new ListNode(1, b2);

        ListNode result = mergeTwoLists(headA, headB);
        while (result != null) {
            System.out.println(result.val);
            result = result.next;
        }
    }

    public static ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        ListNode head = new ListNode(0);
        ListNode curr = head;
        while (list1 != null && list2 != null) {
            if (list1.val <= list2.val) {
                curr.next = list1;
                list1 = list1.next;
            } else {
                curr.next = list2;
                list2 = list2.next;
            }

            curr = curr.next;
        }

        if (list1 != null) {
            curr.next = list1;
        } else if (list2 != null) {
            curr.next = list2;
        }

        return head.next;
    }
}
