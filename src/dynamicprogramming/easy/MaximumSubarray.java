package dynamicprogramming.easy;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/97/dynamic-programming/566/
public class MaximumSubarray {
    public static void main(String[] args) {
        int[] nums = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
//        int[] nums = {1};
//        int[] nums = {5, 4, -1, 7, 8};
        System.out.println(maxSubArray(nums));
    }

    // This is O(n) time complexity
    public static int maxSubArray(int[] nums) {
        int maxSum = nums[0];
        int currentSum = nums[0];

        for (int i = 1; i < nums.length; i++) {
            currentSum = Math.max(nums[i], currentSum + nums[i]);
            maxSum = Math.max(maxSum, currentSum);
        }

        return maxSum;
    }

//    // This is divide-and-conquer technique which has O(n*logn) time complexity
//    public static int maxSubArray(int[] nums) {
//        return maxSubArrayHelper(nums, 0, nums.length - 1);
//    }
//
//    private static int maxSubArrayHelper(int[] nums, int left, int right) {
//        if (left == right) {
//            return nums[left];
//        }
//
//        int mid = (left + right) / 2;
//        int leftMax = maxSubArrayHelper(nums, left, mid);
//        int rightMax = maxSubArrayHelper(nums, mid + 1, right);
//        int crossMax = maxCrossingSubArray(nums, left, mid, right);
//
//        return Math.max(Math.max(leftMax, rightMax), crossMax);
//    }
//
//    private static int maxCrossingSubArray(int[] nums, int left, int mid, int right) {
//        int leftSum = Integer.MIN_VALUE;
//        int sum = 0;
//
//        for (int i = mid; i >= left; i--) {
//            sum += nums[i];
//
//            if (sum > leftSum) {
//                leftSum = sum;
//            }
//        }
//
//        int rightSum = Integer.MIN_VALUE;
//        sum = 0;
//
//        for (int i = mid + 1; i <= right; i++) {
//            sum += nums[i];
//
//            if (sum > rightSum) {
//                rightSum = sum;
//            }
//        }
//
//        return leftSum + rightSum;
//    }
}
