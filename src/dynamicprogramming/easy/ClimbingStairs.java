package dynamicprogramming.easy;

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/97/dynamic-programming/569/
public class ClimbingStairs {
    public static void main(String[] args) {
        int n = 5;
        System.out.println(climbStairs(n));
    }

    // Memoization
    public static int climbStairs(int n) {
        int[] dp = new int[n + 1];
        return climbStairsHelper(dp, n);
    }

    private static int climbStairsHelper(int[] dp, int n) {
        if (n == 1) {
            return 1;
        }

        if (n == 2) {
            return 2;
        }

        if (dp[n] != 0) {
            return dp[n];
        }

        dp[n] = climbStairsHelper(dp, n - 1) + climbStairsHelper(dp, n - 2);
        return dp[n];
    }

//    // Tabulation, O(n) Space complexity
//    public static int climbStairs(int n) {
//        if (n == 1) {
//            return 1;
//        }
//
//        int[] dp = new int[n + 1];
//        dp[1] = 1;
//        dp[2] = 2;
//
//        for (int i = 3; i <= n; i++) {
//            dp[i] = dp[i - 1] + dp[i - 2];
//        }
//
//        return dp[n];
//    }

//    // Tabulation, O(1) Space complexity
//    public static int climbStairs(int n) {
//        if (n == 1) {
//            return 1;
//        }
//
//        int first = 1;
//        int second = 2;
//
//        for (int i = 3; i <= n; i++) {
//            int third = first + second;
//            first = second;
//            second = third;
//        }
//
//        return second;
//    }


}
